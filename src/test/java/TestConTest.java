import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.example.Starter;
import org.example.dto.Student;
import org.example.dto.menu.*;
import org.example.enmu.FortuneEnmu;
import org.example.enmu.GenderColumn;
import org.example.pojo.Menu;
import org.example.pojo.SystemLog;
import org.example.pojo.WxMessage;
import org.example.pojo.WxUser;
import org.example.service.IMenuService;
import org.example.service.ISystemLogService;
import org.example.service.IWxService;
import org.example.utils.AccessTokenUtil;
import org.example.utils.HttpUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  1:03
 */
@SpringBootTest(classes = Starter.class)
@RunWith(SpringRunner.class)
public class TestConTest {

    private static final Logger log = Logger.getLogger(TestConTest.class);

    @Autowired
    private IMenuService menuService;

    @Autowired
    private IWxService wxService;

    @Autowired
    private ISystemLogService logService;

    List<Student> list = Arrays.asList(new Student(1, 18, "阿龙", GenderColumn.BOY.getCode()),
            new Student(2, 17, "小花", GenderColumn.GIRL.getCode()),
            new Student(4, 12, "小花1", GenderColumn.GIRL.getCode()),
            new Student(6, 17, "小花2", GenderColumn.GIRL.getCode()),
            new Student(3, 16, "阿浪", GenderColumn.LADYBOY.getCode()));

    @Test
    public void listToMapByObjectValue1() {
        List<Menu> allMenus = menuService.getAllMenus();
        Map<String, Integer> collect = allMenus.stream().filter(x -> x.getId() > 10).collect(Collectors.toMap(Menu::getName, Menu::getId));
        log.info(allMenus);
    }

    private static final String subscribe = "subscribe";
    private static final String openId = "oLVDO6YEkkFBEn5YSTad5_ysnZ9Q";

    @Test
    public void wxService() {
        List<WxMessage> wxMessages = wxService.qryWxMessageList();
        List<WxMessage> subscribe = wxMessages.stream().filter(wxMessage ->
                wxMessage.getPicUrl() != null
                        && openId.equals(wxMessage.getFromUserName())
        ).collect(Collectors.toList());
        subscribe.forEach(log::info);
        Set<String> userids = wxMessages.stream().map(WxMessage::getFromUserName).collect(Collectors.toSet());
        userids.forEach(log::info);
    }

    @Test
    public void wxUser() {
        List<WxUser> wxUserList = wxService.getAllWxUser();
        wxUserList.forEach(System.out::println);
        System.out.println("------------------------");
        List<WxUser> filterWxUserList = wxUserList.stream().filter(x -> "hys".equals(x.getNickName())).collect(Collectors.toList());
        filterWxUserList.forEach(System.out::println);
        log.info(wxUserList);
    }

    @Test
    public void listToMapByObjectValue() {
        // value 为对象 student -> student jdk1.8返回当前对象
        Map<Integer, Student> map = list.stream().collect(Collectors.toMap(Student::getId, student -> student));
        // 遍历打印结果
        map.forEach((key, value) -> {
            System.out.println("key: " + key + "    value: " + value);
        });
        FortuneEnmu match = FortuneEnmu.match("明年");
    }

    @Test
    public void listToMapByNameValue() {
        // value 为对象中的属性
        Map<Integer, String> map = list.stream().collect(Collectors.toMap(Student::getId, Student::getName));
        map.forEach((key, value) -> {
            System.out.println("key: " + key + "    value: " + value);
        });
    }

    @Test
    public void listToMapByNameValue1() {
        List<Student> list1 = this.list.stream().filter(x ->
                !x.getName().startsWith("小")
                        && x.getAge() > 17
        ).collect(Collectors.toList());
        log.info(list1.toString());
    }

    @Test
    public void test9() {
        List<Student> peopleList = new ArrayList<>();
        peopleList.add(new Student(1, 1, "小王", GenderColumn.GIRL.getCode()));
        peopleList.add(new Student(2, 1, "小李", GenderColumn.BOY.getCode()));
        peopleList.add(new Student(3, 1, "小张", GenderColumn.GIRL.getCode()));
        peopleList.add(new Student(4, 1, "小皇", GenderColumn.GIRL.getCode()));
        List<Student> collect1 = peopleList.stream().filter(x -> x.getId() > 2).collect(Collectors.toList());
        System.out.println(collect1);
        log.debug("collect" + collect1);
    }

    @Test
    public void testbutton() {
        //一级菜单
        OneButton button = new OneButton();
        List<ButtonStrategy> buttons = new ArrayList<>();
        ClickButton clickButton = new ClickButton("何一汕", "1");
        //一级菜单中第二个按钮
        ViewButton viewButton = new ViewButton("啥大笔", "https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Creating_Custom-Defined_Menu.html");
        SubButton subButton = new SubButton("更多");
        List<ButtonStrategy> subButtons = new ArrayList<>();
        //二级菜单第一个按钮
        subButtons.add(new ViewButton("二级菜单1", "https://www.bilibili.com/video/BV1zM411U7gN?p=22&spm_id_from=pageDriver&vd_source=71ee4726179b75aa5f4d5fb52a85ebcf"));
        //二级菜单第二个按钮
        subButtons.add(new PhotoOrAlbumButton("上传图片", "2"));
        subButton.setSub_button(subButtons);
        buttons.add(clickButton);
        buttons.add(viewButton);
        buttons.add(subButton);
        button.setButton(buttons);
        String json = JSONObject.fromObject(button).toString();

        String url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s";
        String accessToken = AccessTokenUtil.getAccessToken();
        url = String.format(url, accessToken);
        String result = HttpUtils.doPostJson(url, json);
        log.debug(result);
    }

    @Test
    public void testSysLog() {
        SystemLog paramDTO = new SystemLog();
        paramDTO.setLogLevel("DEBUG");
        List<SystemLog> list = logService.logList(paramDTO);
        list.forEach(System.out::println);
    }

    @Test
    public void testSysLogPage() {
        SystemLog paramDTO = new SystemLog();
        paramDTO.setLogLevel("INFO");
        paramDTO.setLogMethod("receiveMessage");
        Page<SystemLog> page = logService.logPage(0, 10, paramDTO);
        page.getRecords().forEach(System.out::println);
    }

}
