package org.example.service;

import org.example.pojo.WxMessage;
import org.example.pojo.WxUser;

import java.util.List;
import java.util.Map;

/**
 * @ClassName : IWxService  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/11  16:10
 */

public interface IWxService {
    int check(Map<String, String> map);

    WxUser query(String wxopenid);

    List<WxMessage> qryWxMessageList();

    List<WxUser> getAllWxUser();

}
