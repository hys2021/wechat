package org.example.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.example.pojo.SystemLog;

import java.util.List;

public interface ISystemLogService extends IService<SystemLog> {

    List<SystemLog> logList(SystemLog paramDTO);

    Page<SystemLog> logPage(int current, int limit, SystemLog params);
}
