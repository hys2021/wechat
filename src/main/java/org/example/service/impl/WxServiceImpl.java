package org.example.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.mapper.MenuMapper;
import org.example.mapper.WxMessageMapper;
import org.example.mapper.WxUserMapper;
import org.example.pojo.WxMessage;
import org.example.pojo.WxUser;
import org.example.service.IWxService;
import org.example.utils.DataUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @ClassName : WxServiceImpl  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/11  16:11
 */
@Service
public class WxServiceImpl extends ServiceImpl<WxMessageMapper, WxMessage> implements IWxService {

    @Autowired
    private WxMessageMapper messageMapper;
    @Autowired
    private WxUserMapper userMapper;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public int check(Map<String, String> map) {
        WxMessage wxMessage = JSONObject.parseObject(JSON.toJSONString(map), WxMessage.class);
        wxMessage.setCreateTime(DataUtils.getTodayDateTime());
        ValueOperations<String, Object> redisMap = redisTemplate.opsForValue();
        WxUser wxUser = generate(wxMessage);
        if (StringUtils.isNotBlank(wxMessage.getEvent()) && wxMessage.getEvent().equals("unsubscribe")) {
            userMapper.deleteById(wxUser.getOpenId());
        } else if (StringUtils.isNotBlank(wxMessage.getEvent()) && wxMessage.getEvent().equals("subscribe")) {
            userMapper.insert(wxUser);
            redisTemplate.delete("wxopenid_");
        }
        redisMap.setIfAbsent("wxopenid_", wxUser.getOpenId());
        return messageMapper.insert(wxMessage);
    }

    @Override
    public WxUser query(String wxopenid) {
        return userMapper.selectByOpenId(wxopenid);
    }

    @Override
    public List<WxMessage> qryWxMessageList() {
        return messageMapper.selectList(null);
    }

    @Override
    public List<WxUser> getAllWxUser() {
        return userMapper.selectList(null);
    }

    private WxUser generate(WxMessage wxMessage) {
        WxUser wxUser = new WxUser();
        wxUser.setNickName("hys");
        wxUser.setOpenId(wxMessage.getFromUserName());
        wxUser.setCreateTime(wxMessage.getCreateTime());
        return wxUser;
    }


}
