package org.example.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.mapper.SystemLogMapper;
import org.example.pojo.SystemLog;
import org.example.service.ISystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/25  21:29
 */
@Service
public class SystemLogServiceImpl extends ServiceImpl<SystemLogMapper, SystemLog> implements ISystemLogService {

    @Autowired
    private SystemLogMapper systemLogMapper;

    @Override
    public List<SystemLog> logList(SystemLog params) {
        return systemLogMapper.selectList(getWrapper(params));
    }

    @Override
    public Page<SystemLog> logPage(int current, int limit, SystemLog params) {
        return systemLogMapper.selectPage(new Page<>(current, limit),getWrapper(params));
    }

    /**
     * 获取wrapper
     * @param params
     * @return
     */
    private LambdaQueryWrapper<SystemLog> getWrapper(SystemLog params) {
        LambdaQueryWrapper<SystemLog> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(params.getLogLevel()),SystemLog::getLogLevel, params.getLogLevel());
        wrapper.eq(StringUtils.isNotBlank(params.getLogGenerateClass()),SystemLog::getLogGenerateClass, params.getLogGenerateClass());
        wrapper.eq(StringUtils.isNotBlank(params.getLogMessage()),SystemLog::getLogMessage, params.getLogMessage());
        wrapper.eq(StringUtils.isNotBlank(params.getLogCreate()),SystemLog::getLogCreate, params.getLogCreate());
        wrapper.eq(StringUtils.isNotBlank(params.getId()),SystemLog::getId, params.getId());
        wrapper.eq(StringUtils.isNotBlank(params.getLogMethod()),SystemLog::getLogMethod, params.getLogMethod());
        wrapper.eq(StringUtils.isNotBlank(params.getUserIp()),SystemLog::getUserIp, params.getUserIp());
        wrapper.eq(StringUtils.isNotBlank(params.getUserName()),SystemLog::getUserName, params.getUserName());
        return wrapper;
    }
}
