package org.example.enmu;

import lombok.Getter;
import org.example.strategy.IMessageStrategy;
import org.example.strategy.impl.*;

import java.util.HashMap;
import java.util.Map;

@Getter
public enum MsgEnmu {
    FORTUNEMESSAGE("001", "星座运势", new FortuneMessage()),
    RESPMESSAGE("002", "固定返回", new RespMessage()),
    WORDMESSAGE("003", "同反义词", new WordMessage()),
    FUNYMESSAGE("004", "随机笑话", new FunyMessage()),
    DELETEMENU("004", "删除菜单", new DeleteMenuMessage()),
    ;
    private final String code;
    private final String msg;
    private final IMessageStrategy strategy;

    MsgEnmu(String code, String msg, IMessageStrategy strategy) {
        this.code = code;
        this.msg = msg;
        this.strategy = strategy;
    }

    private static final Map<String, MsgEnmu> mapCodeEnmu = new HashMap<>();
    private static final Map<String, MsgEnmu> mapMsgEnmu = new HashMap<>();

    static {
        for (MsgEnmu value : MsgEnmu.values()) {
            mapCodeEnmu.put(value.getCode(), value);
        }
        for (MsgEnmu value : MsgEnmu.values()) {
            mapMsgEnmu.put(value.getMsg(), value);
        }
    }

    public static MsgEnmu matchCode(String code) {
        return mapCodeEnmu.get(code);
    }

    public static MsgEnmu matchMsg(String code) {
        return mapMsgEnmu.get(code);
    }
}
