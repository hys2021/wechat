package org.example.enmu;


import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
@Getter
public enum FortuneEnmu {

    TODAY("today","今天"),
    TOMORROW("tomorrow","明天"),
    WEEK("week","本周"),
    MONTH("month","本月"),
    YEAR("year","本年")
    ;

    private static final Map<String,FortuneEnmu> mapEnmu=new HashMap<>();
    static {
        for (FortuneEnmu enmu : FortuneEnmu.values()) {
            mapEnmu.put(enmu.data,enmu);
        }
    }
    private final String code;
    private final String data;

    FortuneEnmu(String code, String data) {
        this.code = code;
        this.data = data;
    }

    public static FortuneEnmu match(String data){
        return mapEnmu.get(data);
    }
}
