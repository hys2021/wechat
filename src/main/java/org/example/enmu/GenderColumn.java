package org.example.enmu;

import lombok.Getter;

@Getter
public enum GenderColumn {
    BOY("1","男孩"),
    GIRL("2","女孩"),
    LADYBOY("3","人妖")
    ;

    private final String code;
    private final String data;

    GenderColumn(String code, String data) {
        this.code = code;
        this.data = data;
    }

}
