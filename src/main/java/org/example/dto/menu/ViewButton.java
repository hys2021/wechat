package org.example.dto.menu;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  22:30
 */
@Getter
@Setter
public class ViewButton extends ButtonStrategy {
    public ViewButton(String name,String url) {
        super(name);
        this.type="view";
        this.url=url;
    }
    private String type;
    private String url;
}
