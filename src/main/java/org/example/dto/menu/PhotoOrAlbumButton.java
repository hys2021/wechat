package org.example.dto.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  22:32
 */
@Getter
@Setter
public class PhotoOrAlbumButton extends ButtonStrategy{
    public PhotoOrAlbumButton(String name ,String key) {
        super(name);
        this.type="pic_photo_or_album";
        this.key=key;
    }
    private String type;
    private String key;
}
