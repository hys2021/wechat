package org.example.dto.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  22:27
 */
@Getter
@Setter
public abstract class ButtonStrategy {

    private String name;

    public ButtonStrategy(String name) {
        this.name = name;
    }
}
