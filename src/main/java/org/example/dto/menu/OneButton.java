package org.example.dto.menu;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  22:26
 */
@Getter
@Setter
public class OneButton {
    private List<ButtonStrategy> button;

    public OneButton(List<ButtonStrategy> button) {
        this.button = button;
    }

    public OneButton() {
    }
}
