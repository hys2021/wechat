package org.example.dto.menu;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  22:34
 */
@Getter
@Setter
public class SubButton extends ButtonStrategy{
    public SubButton(String name) {
        super(name);
    }

    private List<ButtonStrategy> sub_button;

}
