package org.example.dto.menu;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  22:29
 */
@Getter
@Setter
public class ClickButton extends ButtonStrategy{


    private String type;
    private String key;

    public ClickButton(String name, String key) {
        super(name);
        this.type = "click";
        this.key = key;
    }
}
