package org.example.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @ClassName : Student  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/11  15:50
 */
@Data
public class Student {
    private Integer id;
    private Integer age;
    private String name;
    private String code;

    public Student(Integer id, Integer age, String name, String code) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.code = code;
    }
}
