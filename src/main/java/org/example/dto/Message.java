package org.example.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @ClassName : Message  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/11  18:16
 */
@Data
@XStreamAlias("xml")
public class Message {

    private String ToUserName;
    private String FromUserName;
    private String CreateTime;
    private String Content;
    private String MsgType;
    private String Image;
    private String voice;
    private String MediaId;

}




