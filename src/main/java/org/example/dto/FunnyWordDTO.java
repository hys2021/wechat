package org.example.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  2:01
 */
@Data
public class FunnyWordDTO {

    private String reason;
    private List<Result> result;
    private Integer error_code;

    @Getter
    @Setter
    public static class Result {
        private String content;
        private String hashId;
        private String unixtime;
    }

}
