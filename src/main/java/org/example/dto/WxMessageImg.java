package org.example.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@Data
@NoArgsConstructor
@AllArgsConstructor
@JacksonXmlRootElement(localName  = "xml")
public class WxMessageImg {
    @JacksonXmlProperty(localName  = "ToUserName")
    private String ToUserName;
    @JacksonXmlProperty(localName  = "FromUserName")
    private String FromUserName;
    @JacksonXmlProperty(localName  = "CreateTime")
    private long  CreateTime;
    @JacksonXmlProperty(localName  = "MsgType")
    private String MsgType;
    @JacksonXmlProperty(localName  = "Event")
    private String Event;
    @JacksonXmlProperty(localName  = "PicUrl")
    private String PicUrl;
    @JacksonXmlProperty(localName  = "MediaId")
    private String MediaId;
    @JacksonXmlProperty(localName  = "MsgId")
    private long  MsgId;
    @JacksonXmlProperty(localName  = "Content")
    private String Content;


}