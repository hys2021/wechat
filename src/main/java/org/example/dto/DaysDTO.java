package org.example.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/12  13:41
 */
@Getter
@Setter
public class DaysDTO {
    private String code;
    private String descirption;

    public DaysDTO(String code, String descirption) {
        this.code = code;
        this.descirption = descirption;
    }
}
