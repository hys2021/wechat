package org.example.controller;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.example.constant.Constant;
import org.example.dto.WxMessageImg;
import org.example.enmu.MsgEnmu;
import org.example.service.IWxService;
import org.example.strategy.MessageContext;
import org.example.utils.WxMessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;


/**
 * @ClassName : WXController  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/11  13:40
 */
@RestController
public class WXController {

    private static final Logger log = Logger.getLogger(WXController.class);
    @Autowired
    private IWxService wxService;
    @Autowired
    private MessageContext messageContext;


    @GetMapping("hello")
    public String hello() {
        return "hello";
    }


    @GetMapping("test")
    public String test() {
        log.info("hello");
        return "hello";
    }

    /**
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return
     */
    @GetMapping("/")
    public String check(String signature, String timestamp, String nonce, String echostr) {
        log.info("signature:" + signature + "timestamp:" + timestamp + ",nonce:" + nonce + ",echostr:" + echostr);
        List<String> arr = Arrays.asList(Constant.TOKEN, timestamp, nonce);
        Collections.sort(arr);
        String checkSignature = arr.stream().map(String::valueOf).collect(Collectors.joining(Constant.NULL));
        MessageDigest instance = null;
        try {
            instance = MessageDigest.getInstance("sha1");
            byte[] digest = instance.digest(checkSignature.getBytes(StandardCharsets.UTF_8));
            StringBuilder sum = new StringBuilder();
            for (byte b : digest) {
                sum.append(Integer.toHexString((b >> 4) & 15));
                sum.append(Integer.toHexString(b & 15));
            }
            if (StringUtils.isNotBlank(signature) && signature.equals(sum.toString())) {
                return echostr;
            }
        } catch (NoSuchAlgorithmException e) {
            log.error("微信公众号校验失败：" + e.getMessage());
        }
        return null;
    }

    @PostMapping("/")
    public String receiveMessage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.debug("request:" + request);
        ServletInputStream inputStream = request.getInputStream();
        Map<String, String> map = new HashMap<>();
        SAXReader reader = new SAXReader();
        //读取输入流 获取Document对象
        try {
            Document document = reader.read(inputStream);
            //获取root节点并将其遍历
            Element root = document.getRootElement();
            //获取所有子节点
            List<Element> elements = root.elements();
            map = elements.stream().collect(Collectors.toMap(Element::getName, Element::getStringValue, (x, y) -> x));
            int i = wxService.check(map);
            log.info("收到的消息：" + map);
        } catch (DocumentException e) {
            log.error("微信公众号消息读取失败：" + e.getMessage());
        }
        String content = map.get("Content");
        String[] split = content.split(Constant.GNA);
        //回复消息
        messageContext.setMessageStrategy(Optional.ofNullable(MsgEnmu.matchMsg(split[0])).orElse(MsgEnmu.RESPMESSAGE).getStrategy());
        return messageContext.getResp(map);
    }


    @PostMapping(value = "analyzeImg2", consumes = "text/xml", produces = "text/xml;charset=utf-8")
    @ResponseBody
    public Object analyzeImg2(@RequestBody WxMessageImg wxMessageImg) {

        //拼一下要返回的信息对象
        WxMessageImg resultMessage = new WxMessageImg();
        try {
            //忽略图片逻辑，直接闹个结果
            String resultStr = "处理完图片返回的信息";
            String openid = wxMessageImg.getFromUserName(); //用户 openid
            String mpid = wxMessageImg.getToUserName();   //公众号原始 ID
            resultMessage.setToUserName(openid);
            resultMessage.setFromUserName(mpid);
            resultMessage.setCreateTime(new Date().getTime());
            resultMessage.setContent(resultStr);
            resultMessage.setMsgType("text");
            //用这个工具类处理出一串玩意直接返回
            String outMesStr = WxMessageUtil.textMessageToXml(resultMessage);
            System.out.println(outMesStr);
            return outMesStr;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
