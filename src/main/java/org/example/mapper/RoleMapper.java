package org.example.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.pojo.Role;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhoubin
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

	/**
	 * 根据用户id查询角色列表
	 * @param adminId
	 * @return
	 */
	List<Role> getRoles(Integer adminId);
}
