package org.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.pojo.Admin;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhoubin
 */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {


	/**
	 * 获取所有操作员
	 * @param keywords
	 * @return
	 */
	List<Admin> getAllAdmins(@Param("id") Integer id, @Param("keywords") String keywords);
}
