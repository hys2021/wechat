package org.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.pojo.WxMessage;

/**
 * @ClassName : WxMessageMapper  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/11  16:11
 */
@Mapper
public interface WxMessageMapper extends BaseMapper<WxMessage> {

}
