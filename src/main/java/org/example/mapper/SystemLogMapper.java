package org.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.pojo.SystemLog;

@Mapper
public interface SystemLogMapper extends BaseMapper<SystemLog> {

}
