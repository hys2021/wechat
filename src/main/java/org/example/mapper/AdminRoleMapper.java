package org.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.example.pojo.AdminRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhoubin
 */
@Mapper
public interface AdminRoleMapper extends BaseMapper<AdminRole> {

	/**
	 * 更新操作员角色
	 * @param adminId
	 * @param rids
	 * @return
	 */
	Integer addAdminRole(@Param("adminId") Integer adminId, @Param("rids") Integer[] rids);
}
