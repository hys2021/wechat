package org.example.constant;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  22:21
 */
public class Constant {
    public static final String TOKEN = "1234qwer";
    public static final String COMMA = ",";
    public static final String GNA = "-";
    public static final String NULL = "";
    public static final String MESSAGE_ID = "001";
}
