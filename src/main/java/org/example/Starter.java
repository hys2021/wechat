package org.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @ClassName : Starter  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/11  13:12
 */
@SpringBootApplication
@MapperScan("org.example.mapper")
@EnableScheduling
public class Starter {
    public static void main(String[] args) {
        SpringApplication.run(Starter.class,args);
    }
}
