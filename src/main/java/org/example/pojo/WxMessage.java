package org.example.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhoubin
 * @since 2023-11-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_wx_message")
public class WxMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "MsgId", type = IdType.AUTO)
    private Long MsgId;

    /**
     * 开发者微信号
     */
    private String ToUserName;

    /**
     * 发送方账号（一个OpenID）
     */
    private String FromUserName;

    /**
     * 文本消息内容
     */
    private String Content;

    /**
     * 消息的数据ID（消息如果来自文章时才有）
     */
    private String MsgDataId;

    /**
     * text
     */
    private String MsgType;

    private String CreateTime;

    private String PicUrl;

    private String MediaId;

    private String Format;

    private String Event;


}
