package org.example.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@TableName(value = "system_log")
public class SystemLog {

    @TableField(value = "Id")
    private String id;
    @TableField(value = "Log_Message")
    private String logMessage;
    @TableField(value = "Log_Level")
    private String logLevel;
    @TableField(value = "Log_Generate_Class")
    private String logGenerateClass;
    @TableField(value = "Log_Create")
    private String logCreate;
    @TableField(value = "Log_Method")
    private String logMethod;
    @TableField(value = "User_Ip")
    private String userIp;
    @TableField(value = "User_Name")
    private String userName;

    @Override
    public String toString() {
        return "SystemLog{" +
                "id=" + id +
                ", logMessage='" + logMessage + '\'' +
                ", logLevel='" + logLevel + '\'' +
                ", logGenerateClass='" + logGenerateClass + '\'' +
                ", logCreate=" + logCreate +
                ", logMethod='" + logMethod + '\'' +
                ", userIp='" + userIp + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }
}
