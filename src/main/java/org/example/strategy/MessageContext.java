package org.example.strategy;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  0:13
 */
@Component
public class MessageContext {
    private IMessageStrategy messageStrategy;

    public void setMessageStrategy(IMessageStrategy messageStrategy) {
        this.messageStrategy = messageStrategy;
    }

    //设置消息
    public String getResp(Map<String, String> map) {
        return messageStrategy.getMessage(map);
    }
}
