package org.example.strategy.impl;

import com.thoughtworks.xstream.XStream;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.example.dto.Message;
import org.example.strategy.IMessageStrategy;
import org.example.utils.DataUtils;
import org.example.utils.HttpUtils;

import java.util.Map;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  0:07
 */
public class WordMessage implements IMessageStrategy {
    private static final Logger log = Logger.getLogger(FortuneMessage.class);

    public static final String WORD_URL = "http://apis.juhe.cn/tyfy/query?key=%s";

    //申请接口的请求key
    public static final String KEY = "f65b1f28333fda2d15d1b75bc2fd1da3";

    @Override
    public String getMessage(Map<String, String> map) {
        Message message = new Message();
        message.setToUserName(map.get("FromUserName"));
        message.setFromUserName(map.get("ToUserName"));
        message.setMsgType(map.get("MsgType"));
        String word = null;
        String type = null;
        if (map.get("MsgType").equals("text")) {
            String content = map.get("Content");
            String[] split = content.split("-");
            for (String s : split) {
                if (s.equals("同义词")) {
                    type = "1";
                } else if (s.equals("反义词")) {
                    type = "2";
                } else {
                    word = s;
                }
            }
            message.setContent(getWord(WORD_URL, KEY, word, type));
        } else if (map.get("MsgType").equals("image")) {
            message.setMediaId("http://mmbiz.qpic.cn/sz_mmbiz_jpg/JibCoJXnUcibcT5xOhGxSVguzY37gChcgjokyyw4KdFfhezjHwWdt3XXb4TiaUCVJzrQgcc6zGrUuqZK4prOTZDdQ/0");
        } else if (map.get("MsgType").equals("voice")) {
            message.setVoice("EseYD7J7hM2L9ty56dkoQuFU16_8D34wPmahKOQmDAXV9ZxjUetlQsaZXxItgtmB");
        }
        message.setCreateTime(DataUtils.getTodayDateTime());
        //将JAVA对象转换为xml
        XStream stream = new XStream();
        stream.processAnnotations(Message.class);
        String xml = stream.toXML(message);
        log.info("返回的消息：" + xml);
        return xml;
    }


    /**
     * 解析回参
     */
    public static String getWord(String wordUrl,String key, String word,String type) {
        //发送http请求的url
        String url = String.format(wordUrl, key);
        String params=String.format("word=%s&type=%s",word,type);
        final String response = HttpUtils.doPost(url, params);
        log.info("接口返回："+ response);
        try {
            JSONObject jsonObject = JSONObject.fromObject(response);
            int error_code = jsonObject.getInt("error_code");
            if (error_code == 0) {
                System.out.println("调用接口成功");
                JSONObject result = jsonObject.getJSONObject("result");
                JSONArray words = result.getJSONArray("words");
                StringBuilder sb = new StringBuilder();
                words.stream().forEach(w -> sb.append(w + " "));
                return sb.toString();
            } else {
                System.out.println("调用接口失败：" + jsonObject.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "null";
    }
}
