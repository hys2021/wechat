package org.example.strategy.impl;

import com.thoughtworks.xstream.XStream;
import org.apache.log4j.Logger;
import org.example.dto.Message;
import org.example.strategy.IMessageStrategy;
import org.example.utils.DataUtils;

import java.util.Map;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  0:08
 */
public class RespMessage implements IMessageStrategy {
    private static final Logger log = Logger.getLogger(FortuneMessage.class);

    @Override
    public String getMessage(Map<String, String> map) {
        Message message = new Message();
        message.setToUserName(map.get("FromUserName"));
        message.setFromUserName(map.get("ToUserName"));
        message.setMsgType(map.get("MsgType"));
        if (map.get("MsgType").equals("text")) {
            message.setContent("欢迎帮助小何测试公众号 ");
        } else if (map.get("MsgType").equals("image")) {
            message.setMediaId("http://mmbiz.qpic.cn/sz_mmbiz_jpg/JibCoJXnUcibcT5xOhGxSVguzY37gChcgjokyyw4KdFfhezjHwWdt3XXb4TiaUCVJzrQgcc6zGrUuqZK4prOTZDdQ/0");
        } else if (map.get("MsgType").equals("voice")) {
            message.setVoice("EseYD7J7hM2L9ty56dkoQuFU16_8D34wPmahKOQmDAXV9ZxjUetlQsaZXxItgtmB");
        }
        message.setCreateTime(DataUtils.getTodayDateTime());
        //将JAVA对象转换为xml
        XStream stream = new XStream();
        stream.processAnnotations(Message.class);
        String xml = stream.toXML(message);
        log.info("返回的消息：" + xml);
        return xml;
    }
}
