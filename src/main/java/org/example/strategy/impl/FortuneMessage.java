package org.example.strategy.impl;

import com.thoughtworks.xstream.XStream;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.example.dto.Message;
import org.example.enmu.FortuneEnmu;
import org.example.strategy.IMessageStrategy;
import org.example.utils.DataUtils;
import org.example.utils.HttpUtils;

import java.util.Map;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  0:02
 */
public class FortuneMessage implements IMessageStrategy {
    private static final Logger log = Logger.getLogger(FortuneMessage.class);

    public static final String WORD_URL = "http://web.juhe.cn/constellation/getAll?key=%s";

    //申请接口的请求key
    public static final String KEY = "31dab4c689c6965b10b558c908cffbd3";

    @Override
    public String getMessage(Map<String, String> map) {
        Message message = new Message();
        message.setToUserName(map.get("FromUserName"));
        message.setFromUserName(map.get("ToUserName"));
        message.setMsgType(map.get("MsgType"));
        if (map.get("MsgType").equals("text")) {
            String content = map.get("Content");
            String[] split = content.split("-");
            String word = split[1];
            FortuneEnmu match = FortuneEnmu.match(split[2]);
            String type = match.getCode();
            message.setContent(getFunnyWord(WORD_URL, KEY, word, type));
        }
        message.setCreateTime(DataUtils.getTodayDateTime());
        //将JAVA对象转换为xml
        XStream stream = new XStream();
        stream.processAnnotations(Message.class);
        String xml = stream.toXML(message);
        log.info("返回的消息：" + xml);
        return xml;
    }

    /**
     * 解析回参
     */
    public static String getFunnyWord(String wordUrl,String key,String word,String type) {
        //发送http请求的url
        String url = String.format(wordUrl, key);
        String params=String.format("consName=%s&type=%s",word,type);
        final String response = HttpUtils.doPost(url, params);
        log.info("接口返回："+ response);
        try {
            JSONObject jsonObject = JSONObject.fromObject(response);
            int error_code = jsonObject.getInt("error_code");
            if (error_code == 0) {
                log.info("调用接口成功");
                return jsonObject.toString();
            } else {
                System.out.println("调用接口失败：" + jsonObject.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "null";
    }
}
