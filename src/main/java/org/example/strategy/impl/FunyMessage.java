package org.example.strategy.impl;

import com.alibaba.fastjson.JSON;
import com.thoughtworks.xstream.XStream;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.example.dto.FunnyWordDTO;
import org.example.dto.Message;
import org.example.strategy.IMessageStrategy;
import org.example.utils.DataUtils;
import org.example.utils.HttpUtils;

import java.util.List;
import java.util.Map;

/**
 * @Author : 何一汕 //作者
 * @Date: 2023/11/17  1:37
 */
public class FunyMessage implements IMessageStrategy {
    private static final Logger log = Logger.getLogger(FunyMessage.class);

    //接口请求地址
    //按更新时间查询笑话
    public static final String URL_A = "http://v.juhe.cn/joke/content/list.php?key=%s&&time=%d&pagesize=%d";

    //最新笑话
    public static final String URL_B = "http://v.juhe.cn/joke/content/text.php?key=%s&pagesize=%d";

    //随机笑话
    public static final String URL_C = "http://v.juhe.cn/joke/randJoke.php?key=%s";

    //申请接口的请求key
    public static final String KEY = "3d54fc3139094e608efb67307b92d824";

    @Override
    public String getMessage(Map<String, String> map) {
        Message message = new Message();
        message.setToUserName(map.get("FromUserName"));
        message.setFromUserName(map.get("ToUserName"));
        message.setMsgType(map.get("MsgType"));
        if (map.get("MsgType").equals("text")) {
            message.setContent(getFunnyWord(URL_C,KEY));
        }
        message.setCreateTime(DataUtils.getTodayDateTime());
        //将JAVA对象转换为xml
        XStream stream = new XStream();
        stream.processAnnotations(Message.class);
        String xml = stream.toXML(message);
        log.info("返回的消息：" + xml);
        return xml;
    }



    /**
     * 解析回参
     */
    public static String getFunnyWord(String wordUrl,String key) {
        //发送http请求的url
        String url = String.format(wordUrl, key);
//        String params=String.format("consName=%s&type=%s",word,type);
        final String response = HttpUtils.doGet(url);
        log.info("接口返回："+ response);
        try {
            FunnyWordDTO funnyWordDTO = JSON.parseObject(response, FunnyWordDTO.class);
            int error_code = funnyWordDTO.getError_code();
            if (error_code == 0) {
                log.info("调用接口成功");
                return funnyWordDTO.getResult().get(0).getContent();
            } else {
                log.error("调用接口失败：" + funnyWordDTO.toString());
            }
        } catch (Exception e) {
            log.error(e);
        }
        return "null";
    }
    /**
     * 随机笑话
     *
     */
    public static void printC() {
        //发送http请求的url
        String url = String.format(URL_C, KEY);
        final String response = HttpUtils.doGet(url);
        System.out.println("接口返回：" + response);
        try {
            JSONObject jsonObject = JSONObject.fromObject(response);
            int error_code = jsonObject.getInt("error_code");
            if (error_code == 0) {
                System.out.println("调用接口成功");
                JSONArray result = jsonObject.getJSONArray("result");
                result.stream().map(JSONObject::fromObject).forEach(hour -> {
                    System.out.println("content：" + ((JSONObject) hour).getString("content"));
                    System.out.println("hashId：" + ((JSONObject) hour).getString("hashId"));
                    System.out.println("unixtime：" + ((JSONObject) hour).getString("unixtime"));
                });

            } else {
                System.out.println("调用接口失败：" + jsonObject.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 最新笑话
     *
     * @param pageSize int 每页数量
     */
    public static void printB( int pageSize) {
        //发送http请求的url
        String url = String.format(URL_B, KEY, pageSize);
        final String response = HttpUtils.doGet(url);
        System.out.println("接口返回：" + response);
        try {
            JSONObject jsonObject = JSONObject.fromObject(response);
            int error_code = jsonObject.getInt("error_code");
            if (error_code == 0) {
                System.out.println("调用接口成功");
                JSONArray result = jsonObject.getJSONObject("result").getJSONArray("data");
                result.stream().map(JSONObject::fromObject).forEach(hour -> {
                    System.out.println("content：" + ((JSONObject) hour).getString("content"));
                    System.out.println("hashId：" + ((JSONObject) hour).getString("hashId"));
                    System.out.println("unixtime：" + ((JSONObject) hour).getString("unixtime"));
                    System.out.println("updatetime：" + ((JSONObject) hour).getString("updatetime"));
                });

            } else {
                System.out.println("调用接口失败：" + jsonObject.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 按更新时间查询笑话
     *
     * @param time     long 时间戳
     * @param pageSize int 每页数量
     */
    public static void printA(long time, int pageSize) {
        //发送http请求的url
        String url = String.format(URL_A, KEY, time, pageSize);

        final String response = HttpUtils.doGet(url);
        System.out.println("接口返回：" + response);
        try {
            JSONObject jsonObject = JSONObject.fromObject(response);
            int error_code = jsonObject.getInt("error_code");
            if (error_code == 0) {
                System.out.println("调用接口成功");
                JSONArray result = jsonObject.getJSONObject("result").getJSONArray("data");
                result.stream().map(JSONObject::fromObject).forEach(hour -> {
                    System.out.println("content：" + ((JSONObject) hour).getString("content"));
                    System.out.println("hashId：" + ((JSONObject) hour).getString("hashId"));
                    System.out.println("unixtime：" + ((JSONObject) hour).getString("unixtime"));
                    System.out.println("updatetime：" + ((JSONObject) hour).getString("updatetime"));
                });

            } else {
                System.out.println("调用接口失败：" + jsonObject.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
