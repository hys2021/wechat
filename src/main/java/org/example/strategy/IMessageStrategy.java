package org.example.strategy;

import java.util.Map;

public interface IMessageStrategy {
    /**
     * 获取消息
     * @param map
     * @return
     */
    public String getMessage(Map<String, String> map);
}
