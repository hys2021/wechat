package org.example.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @ClassName : DataUtils  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/11  18:22
 */

public class DataUtils {
    public static String getTodayDateTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        return format.format(new Date());
    }
}
