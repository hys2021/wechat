package org.example.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;
import org.example.controller.WXController;

/**
 * @ClassName : AccessTokenUtil  //类名
 * @Description :   //描述
 * @Author : 何一汕 //作者
 * @Date: 2023/11/12  12:11
 */

public class AccessTokenUtil {

    private static final Logger log = Logger.getLogger(WXController.class);

    private static final String WX_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

    private static final String APPID = "wxe4feb640cdb468a2";

    private static final String APPSECRET = "c2b6d81aa01765a4f68fcd087d5140e4";

    /**
     * 获取token
     *
     * @return
     */
    public static String getAccessToken() {
        String url = String.format(WX_URL, APPID, APPSECRET);
        log.info("url:"+url);
        String s = HttpUtils.doGet(url);
        JSONObject jsonObject = JSON.parseObject(s);
        return jsonObject.get("access_token").toString();
    }
}
