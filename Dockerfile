FROM openjdk:8
VOLUME /tmp
COPY target/WxTest.jar demo-2.jar
RUN bash -c "touch /demo-2.jar"
EXPOSE 8081
ENTRYPOINT ["java","-jar","demo-2.jar"]